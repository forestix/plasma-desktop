msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-05 01:57+0000\n"
"PO-Revision-Date: 2023-01-11 09:27+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: kksrc sh\n"

#: globalaccelmodel.cpp:209
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Erro ao gravar o atalho %1: %2"

#: globalaccelmodel.cpp:321
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Erro ao adicionar o %1; parece que não tem acções."

#: globalaccelmodel.cpp:364
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Erro ao comunicar com o serviço de atalhos globais"

#: kcm_keys.cpp:57
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Não foi possível comunicar com o serviço de atalhos globais"

#: kcm_keys.cpp:312
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"O atalho %1 já está atribuído à acção comum de %2 '%3'.\n"
"Deseja atribuí-la de novo?"

#: kcm_keys.cpp:316
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"O atalho %1 já está atribuído à acção comum '%2' de %3.\n"
"Deseja atribuí-la de novo?"

#: kcm_keys.cpp:317
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Conflito detectado"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "File"
msgstr "Ficheiro"

#: standardshortcutsmodel.cpp:35
#, kde-format
msgid "Edit"
msgstr "Editar"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "Navigation"
msgstr "Navegação"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "View"
msgstr "Ver"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Settings"
msgstr "Configuração"

#: standardshortcutsmodel.cpp:40
#, kde-format
msgid "Help"
msgstr "Ajuda"

#: ui/main.qml:27
#, kde-format
msgid "Applications"
msgstr "Aplicações"

#: ui/main.qml:27
#, kde-format
msgid "Commands"
msgstr "Comandos"

#: ui/main.qml:27
#, kde-format
msgid "System Settings"
msgstr "Configuração do Sistema"

#: ui/main.qml:27
#, kde-format
msgid "Common Actions"
msgstr "Acções Comuns"

#: ui/main.qml:51
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr ""
"Não é possível exportar o esquema enquanto existirem modificações por gravar"

#: ui/main.qml:63
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"Seleccione os componentes abaixo que devem ser incluídos no esquema exportado"

#: ui/main.qml:69
#, kde-format
msgid "Save scheme"
msgstr "Gravar o esquema"

#: ui/main.qml:169
#, kde-format
msgctxt "@tooltip:button %1 is the text of a custom command"
msgid "Edit command for %1"
msgstr "Editar o comando de %1"

#: ui/main.qml:185
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Remover todos os atalhos do %1"

#: ui/main.qml:196
#, kde-format
msgid "Undo deletion"
msgstr "Desfazer a remoção"

#: ui/main.qml:247
#, kde-format
msgid "No items matched the search terms"
msgstr "Sem itens correspondentes aos termos da pesquisa"

#: ui/main.qml:280
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Seleccione um item na lista para ver aqui os atalhos do mesmo"

#: ui/main.qml:305
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Application…"
msgstr "Adicionar uma Aplicação…"

#: ui/main.qml:315
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Command…"
msgstr "Adicionar um Comando…"

#: ui/main.qml:334
#, kde-format
msgid "Import Scheme…"
msgstr "Importar um Esquema…"

#: ui/main.qml:339
#, kde-format
msgid "Cancel Export"
msgstr "Cancelar a Exportação"

#: ui/main.qml:339
#, kde-format
msgid "Export Scheme…"
msgstr "Exportar o Esquema…"

#: ui/main.qml:359
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Exportar o Esquema de Atalhos"

#: ui/main.qml:359 ui/main.qml:475
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Importar um Esquema de Atalhos"

#: ui/main.qml:361
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Esquema de Atalhos (*.kksrc)"

#: ui/main.qml:389
#, kde-format
msgid "Edit Command"
msgstr "Editar o Comando"

#: ui/main.qml:389
#, kde-format
msgid "Add Command"
msgstr "Adicionar um Comando"

#: ui/main.qml:407
#, kde-format
msgid "Save"
msgstr "Gravar"

#: ui/main.qml:407
#, kde-format
msgid "Add"
msgstr "Adicionar"

#: ui/main.qml:432
#, kde-format
msgid "Enter a command or choose a script file:"
msgstr "Indique um comando ou escolha um ficheiro de programa:"

#: ui/main.qml:446
#, kde-format
msgctxt "@action:button"
msgid "Choose…"
msgstr "Escolher…"

#: ui/main.qml:459
#, kde-format
msgctxt "@title:window"
msgid "Choose Script File"
msgstr "Escolher o Ficheiro do Programa"

#: ui/main.qml:461
#, kde-format
msgctxt "Template for file dialog"
msgid "Script file (*.*sh)"
msgstr "Ficheiro de programa (*.*sh)"

#: ui/main.qml:482
#, kde-format
msgid "Select the scheme to import:"
msgstr "Seleccione o esquema a importar:"

#: ui/main.qml:496
#, kde-format
msgid "Custom Scheme"
msgstr "Esquema Personalizado"

#: ui/main.qml:501
#, kde-format
msgid "Select File…"
msgstr "Seleccionar o Ficheiro…"

#: ui/main.qml:501
#, kde-format
msgid "Import"
msgstr "Importar"

#: ui/ShortcutActionDelegate.qml:30
#, kde-format
msgid "Editing shortcut: %1"
msgstr "A editar o atalho: %1"

#: ui/ShortcutActionDelegate.qml:42
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: ui/ShortcutActionDelegate.qml:55
#, kde-format
msgid "No active shortcuts"
msgstr "Sem atalhos activos"

#: ui/ShortcutActionDelegate.qml:95
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Atalho predefinido"
msgstr[1] "Atalhos predefinidos"

#: ui/ShortcutActionDelegate.qml:97
#, kde-format
msgid "No default shortcuts"
msgstr "Sem atalhos predefinidos"

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "O atalho predefinido %1 está activo."

#: ui/ShortcutActionDelegate.qml:105
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "O atalho predefinido %1 está inactivo."

#: ui/ShortcutActionDelegate.qml:126
#, kde-format
msgid "Custom shortcuts"
msgstr "Atalhos personalizados"

#: ui/ShortcutActionDelegate.qml:151
#, kde-format
msgid "Delete this shortcut"
msgstr "Apagar este atalho"

#: ui/ShortcutActionDelegate.qml:157
#, kde-format
msgid "Add custom shortcut"
msgstr "Adicionar um atalho personalizado"

#: ui/ShortcutActionDelegate.qml:192
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Cancelar a captura do novo atalho"
