# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015, 2016, 2017, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-06 01:55+0000\n"
"PO-Revision-Date: 2023-05-21 23:32+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Poedit 3.2.2\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Je trenutno v uporabi"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Preseli v\n"
"to dejavnost"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Prikaži tudi\n"
"v tej dejavnosti"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Nastavi"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Zaustavi dejavnost"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Zaustavljene dejavnosti:"

#: contents/activitymanager/ActivityManager.qml:120
msgid "Create activity…"
msgstr "Ustvari dejavnosti…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Dejavnosti"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Nastavi dejavnost"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Izbriši"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Oprostite! Prišlo je do napake pri nalaganju %1."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "Kopiraj na odložišče"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "Poglej podrobnosti napake…"

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr "Odpri %1"

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:244
msgid "About"
msgstr "O programu"

#: contents/configuration/AboutPlugin.qml:48
msgid "Send an email to %1"
msgstr "Pošlji e-pošto za %1"

#: contents/configuration/AboutPlugin.qml:62
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Odpri spletišče %1"

#: contents/configuration/AboutPlugin.qml:130
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:148 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Licenca:"

#: contents/configuration/AboutPlugin.qml:151
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Poglej besedilo dovoljenja"

#: contents/configuration/AboutPlugin.qml:165
msgid "Authors"
msgstr "Avtorji"

#: contents/configuration/AboutPlugin.qml:176
msgid "Credits"
msgstr "Zasluge"

#: contents/configuration/AboutPlugin.qml:188
msgid "Translators"
msgstr "Prevajalci"

#: contents/configuration/AboutPlugin.qml:205
msgid "Report a Bug…"
msgstr "Poročaj o hrošču…"

#: contents/configuration/AppletConfiguration.qml:55
msgid "Keyboard Shortcuts"
msgstr "Tipkovne bližnjice"

#: contents/configuration/AppletConfiguration.qml:292
msgid "Apply Settings"
msgstr "Uveljavi nastavitve"

#: contents/configuration/AppletConfiguration.qml:293
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Nastavitve trenutnega modula so se spremenile. Ali želite spremembe "
"uveljaviti ali zavreči?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Uveljavi"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "Prekliči"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Odpri stran z nastavitvami"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Levi gumb"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Desni gumb"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Srednji gumb"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Gumb za premik nazaj"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Gumb za premik naprej"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Navpično drsenje"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Vodoravno drsenje"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Dvigalka"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr "O programu"

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Dodaj dejanje"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr "Spremembe postavitve so dovoljene samo za sistemskega skrbnika"

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Razporeditev:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
msgid "Wallpaper type:"
msgstr "Vrsta slike ozadja:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr "Dobi nove vtičnike…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"Dokler ne bodo uveljavljene spremembe razporeditev, ne bo mogoče izvesti "
"drugih sprememb"

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
msgid "Apply Now"
msgstr "Uveljavi zdaj"

#: contents/configuration/ConfigurationShortcuts.qml:17
msgid "Shortcuts"
msgstr "Bližnjice"

#: contents/configuration/ConfigurationShortcuts.qml:29
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Ta bližnjica bo aktivirala program, kot da bi ga kliknili."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Slika ozadja"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Dejanja miške"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Vnesite sem"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:43
msgid "Panel Settings"
msgstr "Nastavitve plošče"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:49
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Razpni"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as tall as possible"
msgstr "Nastavi ploščo kolikor je mogoče visoko"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as wide as possible"
msgstr "Nastavi ploščo kolikor je mogoče široko"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:62
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Izbriši"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:65
msgid "Remove this panel; this action is undo-able"
msgstr "Odstrani to ploščo; te dejavnosti ni mogoče stornirati"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:89
msgid "Alignment:"
msgstr "Poravnava:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Top"
msgstr "Zgoraj"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Left"
msgstr "Levo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr "Poravnaj nerazpeto ploščo na vrh, ni učinka, če je plošča razpeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr "Poravnaj nerazpeto ploščo na levo, ni učinka, če je plošča razpeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid "Center"
msgstr "V sredini"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:105
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr "Centriraj nerazpeto ploščo, ni učinka, če je plošča razpeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Bottom"
msgstr "Spodaj"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Right"
msgstr "Desno"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr "Poravnaj nerazpeto ploščo na dno, ni učinka, če je plošča razpeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr "Poravnaj nerazpeto ploščo na desno, ni učinka, če je plošča razpeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:142
msgid "Visibility:"
msgstr "Vidnost:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:147
msgid "Always Visible"
msgstr "Vedno viden"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Auto-Hide"
msgstr "Samodejno skrivaj"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:156
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""
"Plošča je skrita, a se pokaže, ko se kazalka dotakne roba zaslona plošče"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:183
msgid "Opacity:"
msgstr "Neprosojnost:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:188
msgid "Always Opaque"
msgstr "Vedno neprosojno"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid "Adaptive"
msgstr "Prilagodljivo"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:197
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr "Plošča je neprosojna, kadar se je dotika kakšno okno in prosojno sicer"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Always Translucent"
msgstr "Vedno prosojno"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:233
msgid "Floating:"
msgstr "Plavajoča:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Floating"
msgstr "Plavajoča"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:239
msgid "Panel visibly floats away from its screen edge"
msgstr "Plošče vidno plava proč od roba zaslona"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Attached"
msgstr "Pripeta"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:249
msgid "Panel is attached to its screen edge"
msgstr "Plošče je pripeta na rob zaslona"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:271
msgid "Focus Shortcut:"
msgstr "Bližnjica osredotočenosti:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:281
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""
"Pritisnite to bližnjico na tipkovnici, da prenesete osredotočenja na ploščo"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Povlecite za spremembo največje višine."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Povlecite za spremembo največje širine."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Dvojni klik za začetno nastavitev."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Povlecite za spremembo najmanjše višine."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Povlecite za spremembo najmanjše širine."

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Povlecite za spremembo pozicije na robu zaslona.\n"
"Dvojno kliknite za začetno nastavitev."

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "Add Widgets…"
msgstr "Dodaj gradnike…"

#: contents/configuration/panelconfiguration/ToolBar.qml:27
msgid "Add Spacer"
msgstr "Dodaj praznino"

#: contents/configuration/panelconfiguration/ToolBar.qml:28
msgid "More Options…"
msgstr "Več nastavitev…"

#: contents/configuration/panelconfiguration/ToolBar.qml:226
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Povlecite za prenos"

#: contents/configuration/panelconfiguration/ToolBar.qml:265
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Uporabite tipke puščic za pomik plošče"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel width:"
msgstr "Širina plošče:"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel height:"
msgstr "Višina plošče:"

#: contents/configuration/panelconfiguration/ToolBar.qml:406
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Zapri"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Upravljanje plošč in namizij"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"Plošče in namizja lahko vlečete okrog, da jih potegnete na različne zaslone."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "Zamenjaj z namizjem na zaslonu %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "Premakni na zaslon %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "Odstrani namizje"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Odstrani pult"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (primarni)"

#: contents/explorer/AppletAlternatives.qml:64
msgid "Alternative Widgets"
msgstr "Drugotni gradniki"

#: contents/explorer/AppletDelegate.qml:167
msgid "Undo uninstall"
msgstr "Razveljavi odstranitev"

#: contents/explorer/AppletDelegate.qml:168
msgid "Uninstall widget"
msgstr "Odstrani gradnik"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Avtor:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "E-pošta:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Odstrani"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Vsi gradniki"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Gradniki"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Dobi nove gradnike…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Kategorije"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets matched the search terms"
msgstr "Noben gradnik se ne ujema z iskalnimi izrazi"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets available"
msgstr "Gradnikov ni na voljo"

#~ msgid "Switch"
#~ msgstr "Preklopi"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Above"
#~ msgstr "Okna zadaj"

#~ msgid ""
#~ "Like \"Auto-Hide\" but the panel remains visible as long as no windows "
#~ "are covering it up"
#~ msgstr ""
#~ "Kot \"Samodejno skrivaj\", a plošča ostane vidna, dokler je ne prekrije "
#~ "nobeno okno"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Below"
#~ msgstr "Okna zadaj"

#~ msgid ""
#~ "Like \"Always Visible\", but maximized and tiled windows go under the "
#~ "panel as though it didn't exist"
#~ msgstr ""
#~ "Kot \"Vedno vidno\", a razpeta okna gredo pod ploščo, kot da ne bi "
#~ "obstajala"

#~ msgid "Windows In Front"
#~ msgstr "Okna v ospredju"

#~ msgid "Aligns the panel"
#~ msgstr "Poravnaj ploščo"

#~ msgid "Center aligns the panel if the panel is not maximized."
#~ msgstr "Sredinsko poravnaj ploščo, če plošča ni razpeta."

#~ msgid ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."
#~ msgstr ""
#~ "Nastavi ploščo vedno skrito, a jo razkrijem kadar miška vstopi v "
#~ "področje, kjer bi bila plošča, če ne bo bila skrita."

#~ msgid ""
#~ "Makes the panel remain visible always but maximized windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."
#~ msgstr ""
#~ "Nastavi ploščo vedno vidno, razen če jo prekrijejo razpeta okna. Je "
#~ "razkrita, ko miška vstopi v področje, kjer bi bila plošča, če ne bi bila "
#~ "prekrita."

#~ msgid "Makes the panel translucent except when some windows touch it."
#~ msgstr "Nastavi ploščo prosojno, razen če se je kakšno okno dotika."

#~ msgid "Makes the panel translucent always."
#~ msgstr "Nastavi ploščo vedno prosojno."

#~ msgid "Makes the panel float from the edge of the screen."
#~ msgstr "Nastavi ploščo plavajočo od roba zaslona."

#~ msgid "Makes the panel remain attached to the edge of the screen."
#~ msgstr "Nastavi ploščo, da ostane pripeta na rob plošče."

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (onemogočen)"

#~ msgid "Appearance"
#~ msgstr "Videz"

#~ msgid "Search…"
#~ msgstr "Poišči…"

#~ msgid "Screen Edge"
#~ msgstr "Rob zaslona"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Kliknite in povlecite gumb na rob zaslona, da premaknete ploščo tja."

#~ msgid "Width"
#~ msgstr "Širina"

#~ msgid "Height"
#~ msgstr "Višina"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Kliknite in povlecite gumb, da spremenite velikost plošče."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Razporeda ni mogoče spremeniti, ko so gradniki zaklenjeni"

#~ msgid "Lock Widgets"
#~ msgstr "Zakleni gradnike"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Ta bližnjica bo omogočila aplet: žarišče tipkovnice bo postavljeno na "
#~ "aplet in če ima ta pojavno okno (npr. začetni meni), se bo to okno odprlo."

#~ msgid "Stop"
#~ msgstr "Zaustavi"

#~ msgid "Activity name:"
#~ msgstr "Ime dejavnosti:"

#~ msgid "Create"
#~ msgstr "Ustvari"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Ali ste prepričani, da želite izbrisati to dejavnost?"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Ok"
#~ msgstr "V redu"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Apply"
#~ msgstr "Uveljavi"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Cancel"
#~ msgstr "Prekliči"
