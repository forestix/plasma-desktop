msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-12 02:01+0000\n"
"PO-Revision-Date: 2023-09-02 02:55\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-desktop/plasma_toolbox_org.kde."
"desktoptoolbox.pot\n"
"X-Crowdin-File-ID: 43333\n"

#: contents/ui/ToolBoxContent.qml:266
#, kde-format
msgid "Choose Global Theme…"
msgstr "选择全局主题…"

#: contents/ui/ToolBoxContent.qml:273
#, kde-format
msgid "Configure Display Settings…"
msgstr "配置显示设置…"

#: contents/ui/ToolBoxContent.qml:294
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "更多"

#: contents/ui/ToolBoxContent.qml:309
#, kde-format
msgid "Exit Edit Mode"
msgstr "退出编辑模式"
