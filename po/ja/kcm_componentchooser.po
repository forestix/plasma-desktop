# Translation of kcmcomponentchooser into Japanese.
# This file is distributed under the same license as the kdebase package.
# Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>, 2002.
# UTUMI Hirosi <utuhiro78@yahoo.co.jp>, 2004.
# Kurose Shushi <md81@bird.email.ne.jp>, 2004.
# Tadashi Jokagi <elf2000@users.sourceforge.net>, 2005.
# Shinichi Tsunoda <tsuno@ngy.1st.ne.jp>, 2005.
# Fumiaki Okushi <fumiaki@okushi.com>, 2006.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007, 2008.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmcomponentchooser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2022-08-12 21:46+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: applicationmodel.cpp:67
#, kde-format
msgid "Other…"
msgstr "その他..."

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "デフォルトのウェブブラウザ"

#: components/componentchooserarchivemanager.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default archive manager"
msgstr "デフォルトのファイルマネージャを選択"

#: components/componentchooserbrowser.cpp:15
#, kde-format
msgid "Select default browser"
msgstr "デフォルトのブラウザを選択"

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr "デフォルトのメールクライアントを選択"

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr "デフォルトのファイルマネージャを選択"

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr "デフォルトのマップを選択"

#: components/componentchooserimageviewer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default image viewer"
msgstr "デフォルトのファイルマネージャを選択"

#: components/componentchoosermusicplayer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default map"
msgid "Select default music player"
msgstr "デフォルトのマップを選択"

#: components/componentchooserpdfviewer.cpp:11
#, fuzzy, kde-format
#| msgid "Select default browser"
msgid "Select default PDF viewer"
msgstr "デフォルトのブラウザを選択"

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr "デフォルトのダイヤラを選択"

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr "デフォルトのターミナルエミュレータを選択"

#: components/componentchoosertexteditor.cpp:15
#, fuzzy, kde-format
#| msgid "Select default terminal emulator"
msgid "Select default text editor"
msgstr "デフォルトのターミナルエミュレータを選択"

#: components/componentchooservideoplayer.cpp:15
#, fuzzy, kde-format
#| msgid "Select default file manager"
msgid "Select default video player"
msgstr "デフォルトのファイルマネージャを選択"

#: ui/ComponentOverlay.qml:22
#, kde-format
msgid "Details"
msgstr ""

#: ui/ComponentOverlay.qml:28
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr ""

#: ui/ComponentOverlay.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr ""

#: ui/ComponentOverlay.qml:50
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr ""

#: ui/ComponentOverlay.qml:59
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr ""

#: ui/ComponentOverlay.qml:65
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr ""

#: ui/ComponentOverlay.qml:73
#, kde-format
msgid "Change file type association manually"
msgstr ""

#: ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""

#: ui/main.qml:45
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr ""

#: ui/main.qml:49
#, kde-format
msgid "Web browser:"
msgstr "ウェブブラウザ:"

#: ui/main.qml:71
#, kde-format
msgid "Email client:"
msgstr "メールクライアント:"

#: ui/main.qml:93
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "ダイヤラ:"

#: ui/main.qml:115
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr ""

#: ui/main.qml:120
#, kde-format
msgid "Image viewer:"
msgstr ""

#: ui/main.qml:144
#, kde-format
msgid "Music player:"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgid "Video player:"
msgstr ""

#: ui/main.qml:189
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr ""

#: ui/main.qml:194
#, kde-format
msgid "Text editor:"
msgstr ""

#: ui/main.qml:216
#, kde-format
msgid "PDF viewer:"
msgstr ""

#: ui/main.qml:238
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr ""

#: ui/main.qml:243
#, kde-format
msgid "File manager:"
msgstr "ファイルマネージャ:"

#: ui/main.qml:265
#, kde-format
msgid "Terminal emulator:"
msgstr "ターミナルエミュレータ:"

#: ui/main.qml:278
#, fuzzy, kde-format
#| msgid "File manager:"
msgid "Archive manager:"
msgstr "ファイルマネージャ:"

#: ui/main.qml:300
#, fuzzy, kde-format
#| msgid "Map:"
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr "マップ:"
