# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2011, 2012, 2020, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2023-03-01 00:35+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "모양"

#: package/contents/ui/ConfigAppearance.qml:24
#, kde-format
msgid "Icon:"
msgstr "아이콘:"

#: package/contents/ui/ConfigAppearance.qml:26
#, kde-format
msgid "Show the current activity icon"
msgstr "현재 활동 아이콘 표시"

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgid "Show the generic activity icon"
msgstr "일반 활동 아이콘 표시"

#: package/contents/ui/ConfigAppearance.qml:42
#, kde-format
msgid "Title:"
msgstr "제목:"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Show the current activity name"
msgstr "현재 활동 이름 표시"

#: package/contents/ui/main.qml:77
#, kde-format
msgctxt "@info:tooltip"
msgid "Current activity is %1"
msgstr "현재 활동: %1"

#: package/contents/ui/main.qml:87
#, kde-format
msgid "Show Activity Manager"
msgstr "활동 관리자 표시"

#: package/contents/ui/main.qml:88
#, kde-format
msgid "Click to show the activity manager"
msgstr "활동 관리자를 표시하려면 클릭하십시오"
